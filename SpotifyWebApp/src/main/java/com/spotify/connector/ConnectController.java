package com.spotify.connector;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;

@Controller
public class ConnectController {
    private final ConnectorService connectorService;

    public ConnectController(ConnectorService connectorService) {
        this.connectorService = connectorService;
    }

    @GetMapping("/login")
    public ResponseEntity getTest() throws IOException {
        connectorService.proceed();
        return ResponseEntity.ok("dummy text");
    }
}

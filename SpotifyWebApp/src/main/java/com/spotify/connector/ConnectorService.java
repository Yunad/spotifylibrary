package com.spotify.connector;

import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ConnectorService {

    private final SpotifyConnector spotifyConnector;

    public ConnectorService(SpotifyConnector spotifyConnector) {
        this.spotifyConnector = spotifyConnector;
    }

    public void proceed() throws IOException {
        spotifyConnector.connect();
    }
}

package com.spotify.connector;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SpotifyConnector {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpotifyConnector.class);
    private static final String CLIENT_ID = "6c16f0dfbc71426faa05c7d8ffb5f455";
    private static final String CLIENT_SECRET = "dbd86da3bfe94dbb8b0fe54945bf20e7";

    public SpotifyConnector() {
        LOGGER.info(this.getClass().getName());
        LOGGER.info("SPOTIFY CONNECTOR");
        System.out.println("SPOTIFY CONNECTOR");
    }

    public String getText() {
        return "SpotifyConnector";
    }

    public void connect() {
        RestTemplate restTemplate = new RestTemplate();
        String authorizationUrl = "https://accounts.spotify.com/authorize?";
        HttpHeaders httpHeaders = restTemplate.headForHeaders(authorizationUrl);
        ResponseEntity<String> response
                = restTemplate.getForEntity(authorizationUrl + "/1", String.class);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(response.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        JsonNode name = root.path("name");
    }
}

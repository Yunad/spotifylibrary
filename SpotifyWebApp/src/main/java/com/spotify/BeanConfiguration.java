package com.spotify;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.spotify.connector")
public class BeanConfiguration {

}
